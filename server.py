import argparse
import time
from pathlib import Path
import threading
import os

from flask import Flask, render_template, request, jsonify

import cv2
import torch
import torch.backends.cudnn as cudnn
from numpy import random
import base64

from models.experimental import attempt_load
from utils.datasets import LoadStreams, LoadImages
from utils.general import check_img_size, check_requirements, check_imshow, non_max_suppression, apply_classifier, \
    scale_coords, xyxy2xywh, strip_optimizer, set_logging, increment_path
from utils.plots import plot_one_box
from utils.torch_utils import select_device, load_classifier, time_synchronized, TracedModel
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app, support_credentials=True)

device = 'cuda' if torch.cuda.is_available() else 'cpu'

def detect(source, weights, device, img_size, iou_thres, conf_thres, view_img):
    webcam = source.camID.isnumeric()

    set_logging()
    device = select_device(device)
    half = False

    model = attempt_load(weights, map_location=device)
    stride = int(model.stride.max())
    imgsz = check_img_size(img_size, s=stride)

    if half:
        model.half()

    if webcam:
        view_img = check_imshow()
        cudnn.benchmark = True
        dataset = LoadStreams(source.camID, img_size=imgsz, stride=stride)
    else:
        dataset = LoadImages(source.camID, img_size=imgsz, stride=stride)

    names = model.module.names if hasattr(model, 'module') else model.names
    colors = [[random.randint(0, 255) for _ in range(3)] for _ in names]

    if device.type != 'cpu':
        model(torch.zeros(1, 3, imgsz, imgsz).to(device).type_as(next(model.parameters())))

    old_img_w = old_img_h = img_size
    old_img_b = 1

    t0 = time.perf_counter()

    for path, img, im0s, vid_cap in dataset:
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()
        img /= 255.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)
        
        if device.type != 'cpu' and (old_img_b != img.shape[0] or old_img_h != img.shape[2] or old_img_w != img.shape[3]):
            old_img_b = img.shape[0]
            old_img_h = img.shape[2]
            old_img_w = img.shape[3]
    
        t1 = time_synchronized()
        with torch.no_grad():
            pred = model(img)[0]
        t2 = time_synchronized()

        pred = non_max_suppression(pred, conf_thres, iou_thres)
        t3 = time_synchronized()

        for i, det in enumerate(pred):
            if webcam:
                p, s, im0, frame = path[i], '%g: ' % i, im0s[i].copy(), dataset.count
            else:
                p, s, im0, frame = path, '', im0s, getattr(dataset, 'frame', 0)

            p = Path(p)
            
            gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]
            if len(det):
                det[:, :4] = scale_coords(img.shape[2:],det[:, :4], im0.shape).round() 

                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()
                    s += f"{n} {names[int(c)]}{'s' * (n>1)}, "

                for *xyxy, conf, cls in reversed(det):
                    label = f'{names[int(cls)]} {conf:.2f}'
                    plot_one_box(xyxy, im0, label=label, color=colors[int(cls)], line_thickness=1)
                    if int(cls) == 0:
                        source.occupied += 1
                    elif int(cls) == 1:
                        source.free += 1

        ret, buffer = cv2.imencode('.jpg', im0)
        #frame = buffer.tobytes()
        frame = base64.b64encode(buffer)
        source.frame = frame.decode()

        # if view_img:
        #         cv2.imshow(source.camID, im0)
        #         if cv2.waitKey(1) & 0xFF == ord('q'):
        #             break

class camThread(threading.Thread):
    def __init__(self, previewName, camID):
        threading.Thread.__init__(self)
        self.previewName = previewName
        self.camID = str(camID)
        self.occupied = 0
        self.free = 0
        self.frame = None
    def run(self):
        print("Starting " + self.previewName)
        with torch.no_grad():
            detect(self, 'best.pt' ,device, img_size=640, iou_thres=0.45, conf_thres=0.2, view_img = True)
        #camPreview(self.previewName, self.camID)

@app.route('/park', methods = ['GET'])
@cross_origin(supports_credentials=True)
def hello_world():

    n_cams = 0
    threads = []
    cond = True #True to fetch from directory, keep it False to capture from webcam

    if(cond):
        i = 0
        directory = 'images'
        for filename in os.listdir(directory):
            f = os.path.join(directory, filename)
            if os.path.isfile(f):
                threads.append(camThread(f'Camera {i}', f))
                i += 1
                n_cams += 1
    else:
        n_cams = 0 #Specify your number of cameras
        for i in range(1, n_cams+1):
            threads.append(camThread(f'Camera {i-1}', i))

    for i in range(n_cams):
        threads[i].start()
    
    for i in range(n_cams):
        threads[i].join()

    d = []
    for i in range(n_cams):
        p = {}
        p['number'] = i
        p['free'] = threads[i].free
        p['occupied'] = threads[i].occupied
        p['frame'] = threads[i].frame
        d.append(p)

    response = jsonify(d)
    # response.headers.add("Access-Control-Allow-Origin", "*")
    # response.headers.add("Access-Control-Allow-Credentials", "true")
    # response.headers.add("Access-Control-Allow-Methods", "GET,PUT,PATCH,POST,DELETE")
    # response.headers.add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    return response

if __name__ == "__main__":
    app.run(debug = True)